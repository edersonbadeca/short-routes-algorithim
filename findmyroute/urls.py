# coding: utf-8


from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView


urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url="/admin/", permanent=True)),

    url(r'^v1/', include('findmyroute.routes', namespace='maps')),

    url(r'^admin/', include(admin.site.urls)),
)