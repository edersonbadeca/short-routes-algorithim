# coding: utf-8

from django.conf.urls import include, patterns, url

urlpatterns = patterns(
    '',
    url(r'^', include("apps.routes.routes", namespace="maps")),
)
