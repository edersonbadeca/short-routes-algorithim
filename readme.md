# Find My Route

#My Motivation
I use django framework to apply my solution, because of it's robustness and scalability. 

# Requirements

 * Python >= 2.7
 * MySQL Server >= 5.6
 
# Installation

To run the project you can use `virtualenv`

## virtualenv

Local install with virtualenv.

Install system requirements on ubuntu:

```
sudo apt-get install mysql-server mysql-client libmysqlclient-dev
```

Create the database and set the correct permissions:

Start mysql `CLI`:

```bash
mysql -u root -p << EOF
```

Create db and set permissions to defalt user:

```sql
CREATE DATABASE findmyroute;
GRANT ALL PRIVILEGES ON *.* TO 'findmyroute'@'localhost' IDENTIFIED BY 'findmyroute' WITH GRANT OPTION;
EOF
```

> NOTES:
> Database user need **permission** to create new databases for test purposes.


Create your local settings:

```
cp dot_env .env
```

> NOTES:
> Review the values and change if necessary.
> You can use [autoenv](https://github.com/kennethreitz/autoenv) to loads the
> environment variables settings automaticaly.

Create a virutalenv:

Create the virtualenv manually:

```
virtualenv .venv
```

Activate the virtualenv manually:

```
source .venv/bin/activate
```

If you are lucky and have [virtualenvwrapper](https://virtualenvwrapper.readthedocs.org/en/latest/) do:

```
mkproject findmyroute
```

If you are using `mkproject` its time to clone the repo:

```
git clone https://edersonbadeca@bitbucket.org/edersonbadeca/wallmart-routes.git .
```

> This create a virtualenv and activate it, as a plus every time you activate
> the environment using `workon findmyroute` it will change to the project directory.
> there are others nice commands like `cdproject` and `cdvirtualenv`.

Install the project requirements:

```
pip -r requirements/local.txt
```
## Database migrations

All migrations SHOULD have a descritive description always use a command like:

```
python manage.py makemigrations --name <description> <app_label>
```

More info [hehe](https://docs.djangoproject.com/en/1.8/ref/django-admin/#django-admin-option---name).

## Database LoadData
If you want to load a initial logistic routes, just run:

```
 ./manage.py loaddata db.json
```


## Tests

We use `pytest` with some nice plugins :)

Reuse DB is enabled default in `findmyroute/apps/pytest.ini` and has the same effect as running with:

```
py.test --reuse-db
```
To run lib tests:

```
py.test 
```

To Run api tests:
```
py.test routes/tests.py
```

> NOTE: If you want to **force** database creation use `--create-db`

for more info take look in [reuse database](http://pytest-django.readthedocs.org/en/latest/database.html?highlight=nomigration#reuse-db-reuse-the-testing-database-between-test-runs).

A mark helper make it easy to customize tests execution isolation.

You can list which markers exist for your test suite:

```
py.test --markers
```

Example marks:

```
@pytest.mark.api
@pytest.mark.models
```

More info see [skipping tests](http://pytest.org/latest/example/simple.html#control-skipping-of-tests-according-to-command-line-option)

If time to run tests increase you can find out which tests are the slowest using:

```
$ py.test --durations=10
```

This will list the 10 slowest tests.

More info in [profiling test duration](http://pytest.org/latest/example/simple.html#profiling-test-duration)

Timeout is set to 3s in `pytest.ini` you can change it using:

```
py.test --timeout=10
```

Alternatively you can mark individual tests as having a timeout:

```
@pytest.mark.timeout(60)
def test_foo():
    pass
```

For more info take a look to [pytest-timeout plugin](https://pypi.python.org/pypi/pytest-timeout)

#API Documentation

REST API
--------

This API is provided to create maps and route and search the short path inner 2 points.

#Using API - Maps

Create a map

Parameters:

```
 "name": {
    "type": "string",
    "required": true,
    "read_only": false,
    "label": "Map name",
    "help_text": "Map Name Eg. Mapa SP",
    "max_length": 36
}

**POST** /v1/maps/

HTTP 201 Created
Content-Type: application/json
Vary: Accept
Allow: GET, POST, HEAD, OPTIONS

{
    "name": "mapa mg"
}
```

List all maps

```
**GET** /v1/maps/

HTTP 200 OK
Content-Type: application/json
Vary: Accept


[
    {
        "name": "mapa sp"
    },
    {
        "name": "mapa rj"
    }
]
```

Get a Map
```
**GET** /v1/maps/<map_name>/

HTTP 200 OK
Content-Type: application/json
Vary: Accept


{
    "name": "mapa sp"
}
```

Delete a Map
```
**DELETE** /v1/maps/<map_name>/

HTTP 204 No Content
Content-Type: application/json
Vary: Accept

```

Put a map
```
**PUT** /v1/maps/<map_name>/
HTTP 200 OK
Content-Type: application/json
Vary: Accept
Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

{
    "name": "mapas sp"
}

```


#Using API - Routes

The Routes resources has the same operation of Maps. The resource is reached by map_name:
/v1/maps/<map_name>/routes/

Example:

List Map Routes
```
**GET** /v1/maps/<map_name>/routes/

HTTP 200 OK
Content-Type: application/json
Vary: Accept
Allow: GET, POST, HEAD, OPTIONS

[
    {
        "route": "A B 10",
        "map_code": 1
    }
]
```


#Search for short_path:

Parameters:

```
 "mapa": {
    "type": "string",
    "required": true,
    "read_only": True,
    "label": "Map name",
    "help_text": "Map Name Eg. Mapa SP",
},
 "origem": {
    "type": "string",
    "required": true,
    "read_only": True,
    "label": "starting point",
    "help_text": "A",
},
"destino": {
    "type": "string",
    "required": true,
    "read_only": True,
    "label": "destination point",
    "help_text": "A",
},
"autonomia": {
    "type": "string",
    "required": true,
    "read_only": True,
    "label": "autonomy",
    "help_text": "10",
},
"valor_litro": {
    "type": "string",
    "required": true,
    "read_only": True,
    "label": "gas price",
    "help_text": "2.50",
},

**GET** /v1/maps/?mapa=mapa%20rj&origem=A&destino=D&autonomia=10&valor_litro=2.50
HTTP 200 OK
Content-Type: application/json
Vary: Accept
[
	["A",
	"B",
	"D"]
	,6.25
]
```