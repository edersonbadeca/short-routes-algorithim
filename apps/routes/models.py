from django.db import models

from django.utils.translation import gettext as _
from model_utils.models import TimeStampedModel


class Maps(TimeStampedModel):
    name = models.CharField(max_length=36,
                            blank=False,
                            unique=True,
                            verbose_name=_("Map name"),
                            help_text=_("Map Name Eg. Mapa SP"))


class Routes(TimeStampedModel):
    map_code = models.ForeignKey("Maps",
                                 verbose_name="map name",
                                 related_name='map')
    route = models.CharField(max_length=255,
                             blank=False,
                             verbose_name=_("Route"),
                             help_text=_("Route including Km Eg. A B 10"))
