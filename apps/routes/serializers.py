# coding: utf-8

from rest_framework import serializers

from .models import Routes, Maps


class MapsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Maps
        fields = ('name', )


class RouteSerializer(serializers.ModelSerializer):
    maps = MapsSerializer(many=True, read_only=True)

    class Meta:
        model = Routes
        fields = ('route', 'map_code', 'maps')
