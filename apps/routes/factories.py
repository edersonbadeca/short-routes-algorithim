# coding: utf-8

import factory


from models import Maps, Routes


class MapFactory(factory.DjangoModelFactory):
    class Meta:
        model = Maps

    name = factory.Sequence(lambda n: 'map{0}'.format(n))


class RoutesFactory(factory.DjangoModelFactory):
    class Meta:
        model = Routes

    route = factory.Sequence(lambda n, x, j: '{0} {1} {2}'.format(n, x, range(10, 50)))
