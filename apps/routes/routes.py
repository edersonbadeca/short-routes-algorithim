# coding: utf-8

from rest_framework_extensions.routers import ExtendedDefaultRouter

from apps.routes.views import FindMyRouteViewSet, MapsViewSet

router = ExtendedDefaultRouter()
(
    router.register(r'maps', MapsViewSet, base_name='maps')
          .register(r'routes', FindMyRouteViewSet, base_name='routes', parents_query_lookups=["maps__name"])
)

urlpatterns = router.urls
