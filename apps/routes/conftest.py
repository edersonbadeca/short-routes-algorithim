import pytest

from rest_framework.authtoken.models import Token

from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType

@pytest.fixture()
def api_client(db):
    """A Django test API client."""
    from django.test.client import Client

    
    client = Client()
    return client

