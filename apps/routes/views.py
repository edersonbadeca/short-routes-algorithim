# coding: utf-8

from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from rest_framework.response import Response

from .models import Routes, Maps
from .serializers import RouteSerializer, MapsSerializer

from apps.libs.dijkstar import Graph, find_path
from apps.libs.dijkstar.algorithm import NoPathError


class FindMyRouteViewSet(ModelViewSet):
    queryset = Routes.objects.all()
    serializer_class = RouteSerializer
    lookup_field = 'map'

class MapsViewSet(ModelViewSet):
    queryset = Maps.objects.all()
    serializer_class = MapsSerializer
    lookup_field = 'name'

    def list(self, request):
        queryset = Maps.objects.all()
        serializer = MapsSerializer(queryset, many=True)
        if not self.request.query_params.keys():
            return Response(serializer.data)

        map_name = self.request.query_params.get('mapa', None)
        starting_point = self.request.query_params.get('origem', None)
        destination_point = self.request.query_params.get('destino', None)
        autonomy = self.request.query_params.get('autonomia', None)
        gas_price = float(self.request.query_params.get('valor_litro', None))

        if autonomy is None or not autonomy.isdigit():
            return Response('filters are missing..',  status=status.HTTP_400_BAD_REQUEST)

        if starting_point is None:
            return Response('filters are missing..',  status=status.HTTP_400_BAD_REQUEST)

        if destination_point is None:
            return Response('filters are missing..',  status=status.HTTP_400_BAD_REQUEST)

        if map_name is not None:
            graph = Graph()
            _map = Maps.objects.filter(name=map_name)
            routes = Routes.objects.filter(map_code=_map.first)

            for route in routes.all():
                points = route.route
                n1, n2, cost = points.split()
                graph.add_edge(n1, n2, {"cost": int(cost)})

            cost_func = lambda u, v, e, prev_e: e['cost']
            distance = lambda p1, p2: find_path(graph, p1, p2, cost_func=cost_func)

            try:
                short_path = distance(starting_point, destination_point)

                walk_per_liter = float(short_path[-1]) / float(autonomy)
                my_cost = float(walk_per_liter)*float (gas_price)
                result = []
                result.append(short_path[0])
                result.append(my_cost)
                serializer = MapsSerializer(queryset, many=True)
                return Response(result)
            except NoPathError as ex:
                return Response(ex.message,  status=status.HTTP_404_NOT_FOUND)

            except ValueError:
                return Response('filters are missing..',  status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data)
