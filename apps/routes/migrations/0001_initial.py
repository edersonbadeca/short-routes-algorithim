# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Maps',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('name', models.CharField(help_text=b'Map Name Eg. Mapa SP', unique=True, max_length=36, verbose_name=b'Map name')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Routes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('route', models.CharField(help_text=b'Route including Km Eg. A B 10', max_length=255, verbose_name=b'Route')),
                ('map_code', models.ForeignKey(related_name='map', verbose_name=b'map name', to='routes.Maps')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
