import uuid
import urllib

from rest_framework import status

from .factories import MapFactory
import pytest

pytestmark = pytest.mark.django_db


def test_get_maps(api_client):
    url = '/v1/maps/'
    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.data == []

    MapFactory()
    MapFactory()
    MapFactory()
    MapFactory()
    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) > 1


def test_get_map(api_client):

    map_obj = MapFactory()
    url = '/v1/maps/{}/'.format(map_obj.name)

    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert response.data['name'] == map_obj.name


def test_create_map(api_client):
    url = '/v1/maps/'
    data = {'name': uuid.uuid4().hex}
    response = api_client.post(url, data=data)

    assert response.status_code == status.HTTP_201_CREATED


def test_create_route(api_client):
    map_obj = MapFactory()
    url = '/v1/maps/{}/routes/'.format(map_obj.name)
    data = {'route': 'A B 10', 'map_code': map_obj.id}

    response = api_client.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED


def test_search_short_path(api_client):

    map_obj = MapFactory()
    url = '/v1/maps/{}/routes/'.format(map_obj.name)

    data = {'route': 'A B 10', 'map_code': map_obj.id}
    response = api_client.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED

    data = {'route': 'B D 15', 'map_code': map_obj.id}
    response = api_client.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED

    data = {'route': 'A C 20', 'map_code': map_obj.id}
    response = api_client.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED

    data = {'route': 'C D 30', 'map_code': map_obj.id}
    response = api_client.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED

    data = {'route': 'B E 50', 'map_code': map_obj.id}
    response = api_client.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED

    data = {'route': 'D E 30', 'map_code': map_obj.id}
    response = api_client.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED

    url_search = '/v1/maps/?mapa={}&origem={}&destino={}&autonomia={}&valor_litro={}'.format(
        map_obj.name, 'A', 'D', '10', '2.50')

    response = api_client.get(url_search)
    assert response.status_code == status.HTTP_200_OK
    assert response.content == '[["A","B","D"],6.25]'
